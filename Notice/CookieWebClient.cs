﻿using System;
using System.Net;

namespace wang.chuangen
{
    public class CookieWebClient : WebClient
    {
        private CookieContainer cookies = new CookieContainer();

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            HttpWebRequest httpRequest = request as HttpWebRequest;
            if (httpRequest != null) httpRequest.CookieContainer = cookies;
            return request;
        }
    }
}

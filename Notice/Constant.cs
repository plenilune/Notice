using System;
using System.Collections.Specialized;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using wang.chuangen.Properties;

namespace wang.chuangen
{
    public class Constant
    {
        private static byte[] CHECK_PASSWORD = Encoding.UTF8.GetBytes("changepassword");
        private static byte[] CHECK1 = Encoding.UTF8.GetBytes("<script>");
        private static byte[] CHECK2 = Encoding.UTF8.GetBytes("login");
        private static MD5 MD5_CONVERTER = new MD5CryptoServiceProvider();
        private static WebClient WEB_CLIENT = new CookieWebClient();
        private const string configApi = "index.php?mode=getconfig";
        
        public static string BASE_ADDRESS { get; set; }

        public static string ZENTAO_URL { get; set; }

        public static string ACCOUNT { get; set; }

        public static string PASSWORD { get; set; }

        public static int FLUSH_INTERVAL { get; set; }

        public static bool FLUSH_OPENED { get; set; }

        public static bool FLUSH_BUG { get; set; }

        public static bool FLUSH_TASK { get; set; }

        public static bool FLUSH_STORY { get; set; }

        public static ConfigInfo CONFIG_INFO { get; set; }

        private static Api API { get; set; }

        /// <summary>
        /// 初始化配置信息
        /// </summary>
        static Constant()
        {
            Settings DEFAULT = Properties.Settings.Default;
            ZENTAO_URL = DEFAULT.zentaoUrl;
            ACCOUNT = DEFAULT.account;
            PASSWORD = DEFAULT.password;
            FLUSH_INTERVAL = DEFAULT.flushInterval;
            FLUSH_OPENED = DEFAULT.flushOpened;
            FLUSH_BUG = DEFAULT.flushBug;
            FLUSH_TASK = DEFAULT.flushTask;
            FLUSH_STORY = DEFAULT.flushStory;
            if (isNotEmpty(ZENTAO_URL))
            {
                WEB_CLIENT.Encoding = Encoding.UTF8;
                WEB_CLIENT.BaseAddress = BASE_ADDRESS = compileBaseAddress(ZENTAO_URL);
            }
        }

        /// <summary>
        /// 判断是否配置禅道信息
        /// </summary>
        /// <returns></returns>
        public static bool isConfigOK()
        {
            return isNotEmpty(BASE_ADDRESS);
        }

        /// <summary>
        /// 获取api接口
        /// </summary>
        /// <param name="baseAddress">禅道路径</param>
        /// <returns></returns>
        private static ConfigInfo config(string baseAddress)
        {
            try
            {
                byte[] result = WEB_CLIENT.DownloadData(uri(baseAddress, configApi));
                return ConfigInfo.convert(result);
            }
            catch (Exception)
            {
                MessageBox.Show("无法连接到禅道地址:" + baseAddress, "错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return null;
        }

        /// <summary>
        /// 登录用户
        /// </summary>
        /// <param name="configInfo"></param>
        /// <param name="baseAddress"></param>
        /// <param name="account"></param>
        /// <param name="password"></param>
        /// <param name="success"></param>
        /// <param name="failed"></param>
        private static void login(ConfigInfo configInfo, string baseAddress, string account, string password, Action success, Action failed)
        {
            NameValueCollection data = new NameValueCollection();
            data.Add("account", account);
            data.Add("password", textMD5(password + configInfo.rand));
            Api api = configInfo.generateApi();
            byte[] buffer = WEB_CLIENT.UploadValues(uri(baseAddress, api.getApi("user", "login")), data);
            ResultInfo result = ResultInfo.convert(buffer);
            if (result != null && "success".Equals(result.status, StringComparison.CurrentCultureIgnoreCase) && result.user != null)
            {
                WEB_CLIENT.BaseAddress = Constant.BASE_ADDRESS = baseAddress;
                Constant.CONFIG_INFO = configInfo;
                Constant.API = api;
                success();
            }
            else
            {
                MessageBox.Show("登录失败!", "提示", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                failed();
            }
        }

        /// <summary>
        /// 登录用户
        /// </summary>
        /// <param name="baseAddress">根路径</param>
        /// <param name="account">账号</param>
        /// <param name="password">密码</param>
        /// <param name="success">成功</param>
        /// <param name="failed">失败</param>
        public static void login(string baseAddress, string account, string password, Action success, Action failed)
        {
            ConfigInfo configInfo = config(baseAddress);
            if (configInfo != null)
            {
                login(configInfo, baseAddress, account, password, success, failed);
            }
            else
            {
                failed();
            }
        }

        /// <summary>
        /// 重新登录
        /// </summary>
        /// <param name="action">登录成功回调事件</param>
        public static void relogin(Action action)
        {
            login(BASE_ADDRESS, ACCOUNT, PASSWORD, action, emptyAction);
        }

        /// <summary>
        /// 打开浏览器
        /// </summary>
        /// <param name="data"></param>
        public static void openView(Data data)
        {
            System.Diagnostics.Process.Start(BASE_ADDRESS + API.getWebUrl(data.view, data.id, CONFIG_INFO.sessionName, CONFIG_INFO.sessionID));
        }

        /// <summary>
        /// 请求service
        /// </summary>
        /// <param name="module">模块</param>
        /// <param name="method">方法</param>
        /// <param name="type">类型</param>
        /// <param name="view">view</param>
        /// <param name="action">回调事件</param>
        public static void service(string module, string method, string type, Action<Result> action)
        {
            if (API != null)
            {
                service(uri(BASE_ADDRESS, API.getApi(module, method, type)), buffer => action(convertResult(buffer)));
            }
            else
            {
                relogin(() => service(module, method, type, action));
            }
        }

        /// <summary>
        /// 请求service
        /// </summary>
        /// <param name="module">模块</param>
        /// <param name="method">方法</param>
        /// <param name="view">view</param>
        /// <param name="action">回调事件</param>
        public static void service(string module, string method, Action<Result> action)
        {
            if (API != null)
            {
                service(uri(BASE_ADDRESS, API.getApi(module, method)), buffer => action(convertResult(buffer)));
            }
            else
            {
                relogin(() => service(module, method, action));
            }
        }

        /// <summary>
        /// 请求service
        /// </summary>
        /// <param name="uri">uri</param>
        /// <param name="action">回调事件</param>
        private static void service(Uri uri, Action<byte[]> action)
        {
            service(uri, action, () => relogin(() => service(uri, action, emptyAction)));
        }

        /// <summary>
        /// 请求service
        /// </summary>
        /// <param name="uri">uri</param>
        /// <param name="action">回调事件</param>
        /// <param name="logout">登录超时事件</param>
        private static void service(Uri uri, Action<byte[]> action, Action logout)
        {
            try
            {
                byte[] buffer = WEB_CLIENT.DownloadData(uri);
                if (!checkLogout(buffer))
                {
                    action(buffer);
                }
                else
                {
                    logout();
                }
            }
            catch (Exception)
            {
            }
            
        }

        /// <summary>
        /// 转换结果值
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        private static Result convertResult(byte[] buffer)
        {
            ResultInfo result = ResultInfo.convert(buffer);
            if (result != null && isNotEmpty(result.data))
            {
                buffer = Encoding.UTF8.GetBytes(result.data);
                if (result.data.IndexOf('[') > 0)
                {
                    return Result.convert(buffer);
                }
                else
                {
                    return ResultDeviant.convert(buffer);
                }
            }
            return null;
        }

        /// <summary>
        /// 检查登录超时
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        private static bool checkLogout(byte[] buffer)
        {
            return containsBytes(buffer, CHECK1) && containsBytes(buffer, CHECK2);
        }

        /// <summary>
        /// 生成url
        /// </summary>
        /// <param name="baseAddress"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        private static Uri uri(string baseAddress, string url)
        {
            try
            {
                return new Uri(baseAddress + url);
            }
            catch (Exception)
            {
                MessageBox.Show("URL地址错误:" + baseAddress + url, "错误", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return null;
        }

        /// <summary>
        /// 处理baseAddress
        /// </summary>
        /// <param name="baseAddress"></param>
        /// <returns></returns>
        public static string compileBaseAddress(string zentaoUrl)
        {
            zentaoUrl = trimToNull(zentaoUrl);
            if (zentaoUrl != null)
            {
                if (!zentaoUrl.StartsWith("htt"))
                {
                    zentaoUrl = "http://" + zentaoUrl;
                }
                if (!zentaoUrl.EndsWith("/"))
                {
                    zentaoUrl += "/";
                }
                return zentaoUrl;
            }
            return null;
        }

        /// <summary>
        /// 判断一个byte数组中是否包含指定数组内容
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool containsBytes(byte[] buffer, byte[] value)
        {
            if (buffer != null && buffer.Length > 0 && value != null && value.Length > 0)
            {
                int i = 0;
                int j = 0;
                int bs = buffer.Length;
                int vs = value.Length;

                flag:if (i < bs)
                {
                    for (j = 0; j < vs; j++)
                    {
                        if (buffer[i + j] != value[j])
                        {
                            i++;
                            goto flag;
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string textMD5(string text)
        {
            if (isNotEmpty(text))
            {
                byte[] buffer = Encoding.Default.GetBytes(text.Trim());
                buffer = MD5_CONVERTER.ComputeHash(buffer);
                return BitConverter.ToString(buffer).Replace("-", "").ToLowerInvariant();
            }
            return null;
        }

        /// <summary>
        /// 去前后空格
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string trimToNull(string value)
        {
            if (value != null)
            {
                value = value.Trim();
                if (value.Length > 0)
                {
                    return value;
                }
            }
            return null;
        }

        /// <summary>
        /// 非空值判断
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool isNotEmpty(string value)
        {
            if (value != null && value.Trim().Length > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 空值判断
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool isEmpty(string value)
        {
            return !isNotEmpty(value);
        }

        /// <summary>
        /// 空方法
        /// </summary>
        public static void emptyAction() { }

        /// <summary>
        /// 销毁web客户端
        /// </summary>
        public static void Dispose()
        {
            WEB_CLIENT.Dispose();
            WEB_CLIENT = null;
        }
    }
}

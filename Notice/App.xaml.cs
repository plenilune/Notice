﻿using System.Windows;
using wang.chuangen.Properties;

namespace wang.chuangen
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {

        /// <summary>
        /// 应用启动事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onStartup(object sender, StartupEventArgs e)
        {
            string[] args = e.Args;
            if (args != null && args.Length > 0)
            {
                string command = args[0];
                if ("reset".Equals(command, System.StringComparison.CurrentCultureIgnoreCase))
                {
                    Constant.BASE_ADDRESS = null;
                }
            }
        }
    }
}

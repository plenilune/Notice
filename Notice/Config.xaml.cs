﻿using System;
using System.Windows;
using wang.chuangen.Properties;

namespace wang.chuangen
{
    /// <summary>
    /// Config.xaml 的交互逻辑
    /// </summary>
    public partial class Config : Window
    {
        private string zentaoUrlText;
        private string accountText;
        private string passwordText;
        private Action action;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="action"></param>
        public Config(Window parent, Action action)
        {
            InitializeComponent();
            this.Owner = parent;
            this.action = action;
            zentaoUrl.Text = zentaoUrlText = Constant.ZENTAO_URL;
            account.Text = accountText = Constant.ACCOUNT;
            password.Password = passwordText = Constant.PASSWORD;
            if (Constant.FLUSH_INTERVAL > 0)
            {
                flushInterval.SelectedValue = Constant.FLUSH_INTERVAL;
            }
            flushOpened.IsChecked = Constant.FLUSH_OPENED;
            flushBug.IsChecked = Constant.FLUSH_BUG;
            flushTask.IsChecked = Constant.FLUSH_TASK;
            flushStory.IsChecked = Constant.FLUSH_STORY;
        }

        /// <summary>
        /// 窗口激活事件
        /// </summary>
        /// <param name="parent"></param>
        private void onActivated(object obj, EventArgs e)
        {
            double workWidth = SystemParameters.WorkArea.Width;
            double workHeight = SystemParameters.WorkArea.Height;
            double formWidth = ActualWidth;
            double formHeight = ActualHeight;
            double parentTop = Owner.Top;
            double parentHeight = Owner.Height;
            double x = Owner.Left;
            if ((x + formWidth) > (workWidth * 0.9))
            {
                x = (x + Owner.ActualWidth - formWidth);
            }
            double y = parentTop + parentHeight;
            if ((y + formHeight) > (workHeight * 0.9))
            {
                y = parentTop - formHeight;
            }
            Left = x;
            Top = y;
        }

        /// <summary>
        /// 保存事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onSaveConfig(object sender, RoutedEventArgs e)
        {
            zentaoUrlText = zentaoUrl.Text;
            if (Constant.isEmpty(zentaoUrlText))
            {
                MessageBox.Show("禅道地址不能为空!", "提示", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            accountText = account.Text;
            if (Constant.isEmpty(accountText))
            {
                MessageBox.Show("账号不能为空!", "提示", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            string thePasswordText = password.Password;
            if (Constant.isEmpty(thePasswordText))
            {
                MessageBox.Show("密码不能为空!", "提示", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (passwordText == null || !passwordText.Equals(thePasswordText))
            {
                passwordText = Constant.textMD5(thePasswordText);
            }
            IsEnabled = false;
            string baseAddress = Constant.compileBaseAddress(zentaoUrlText);
            Constant.login(baseAddress, accountText, passwordText, action + configSuccess, configFailed);
        }

        /// <summary>
        /// 配置成功,保存数据
        /// </summary>
        private void configSuccess()
        {
            this.DialogResult = true;
            Settings defaultSertings = Properties.Settings.Default;
            defaultSertings.zentaoUrl = Constant.ZENTAO_URL = zentaoUrlText;
            defaultSertings.account = Constant.ACCOUNT = accountText;
            defaultSertings.password = Constant.PASSWORD = passwordText;
            defaultSertings.flushInterval = Constant.FLUSH_INTERVAL = intValue(flushInterval.SelectedValue);
            defaultSertings.flushOpened = Constant.FLUSH_OPENED = (flushOpened.IsChecked == true);
            defaultSertings.flushBug = Constant.FLUSH_BUG = (flushBug.IsChecked == true);
            defaultSertings.flushTask = Constant.FLUSH_TASK = (flushTask.IsChecked == true);
            defaultSertings.flushStory = Constant.FLUSH_STORY = (flushStory.IsChecked == true);
            defaultSertings.Save();
            Close();
        }

        /// <summary>
        /// 配置失败,恢复窗口
        /// </summary>
        private void configFailed()
        {
            IsEnabled = true;
        }

        /// <summary>
        /// int
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int intValue(object value)
        {
            try
            {
                if (value != null)
                {
                    return Convert.ToInt32(value);
                }
                return 0;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Windows;

namespace wang.chuangen
{

    public class Result
    {
        private static DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Result));

        public string title { get; set; }

        public ICollection<Data> items { get; set; }

        public ICollection<Data> bugs { get { return items; } set { items = value; } }

        public ICollection<Data> tasks { get { return items; } set { items = value; } }

        public ICollection<Data> stories { get { return items; } set { items = value; } }

        public Pager pager { get; set; }

        public Visibility visibility { get; set; }

        /// <summary>
        /// 解析结果数据
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static Result convert(byte[] buffer)
        {
            try
            {
                if (buffer != null && buffer.Length > 0)
                {
                    return serializer.ReadObject(new MemoryStream(buffer)) as Result;
                }
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }
    }
}

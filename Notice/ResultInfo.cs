﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;

namespace wang.chuangen
{
    public class ResultInfo
    {

        private static DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ResultInfo));

        public string status { get; set; }

        public User user { get; set; }

        public string data { get; set; }

        public string reason { get; set; }

        public string md5 { get; set; }

        /// <summary>
        /// 解析结果数据
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static ResultInfo convert(byte[] buffer)
        {
            try
            {
                if (buffer != null && buffer.Length > 0)
                {
                    return serializer.ReadObject(new MemoryStream(buffer)) as ResultInfo;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}

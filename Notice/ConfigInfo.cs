﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;

namespace wang.chuangen
{
    public class ConfigInfo
    {
        /// <summary>
        /// 序列化工具类
        /// </summary>
        private static DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ConfigInfo));

        public string requestType { get; set; }

        public string sessionName { get; set; }

        public string sessionID { get; set; }

        public int rand { get; set; }

        /// <summary>
        /// 生成API
        /// </summary>
        /// <returns></returns>
        public Api generateApi()
        {
            if ("PATH_INFO".Equals(requestType, StringComparison.CurrentCultureIgnoreCase))
            {
                return new PathInfoApi();
            }
            else
            {
                return new GetApi();
            }
        }

        /// <summary>
        /// 解析json数据
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static ConfigInfo convert(byte[] buffer)
        {
            try
            {
                if (buffer != null && buffer.Length > 0)
                {
                    return serializer.ReadObject(new MemoryStream(buffer)) as ConfigInfo;  
                }
            }
            catch (Exception)
            {
            }
            return null;
        }
    }
}

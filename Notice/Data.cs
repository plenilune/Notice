﻿using System.Windows.Media;
namespace wang.chuangen
{

    public class Data
    {

        public string id { get; set; }

        public string title { get; set; }

        public string name { get { return title; } set { title = value; } }

        public string pri { get; set; }

        public string view { get; set; }

        public string status { get; set; }

        public string content { get { return "[" + id + "] " + title; } }

        public Brush color
        {
            get
            {
                switch (pri)
                {
                    case "1": return Brushes.Red;
                    case "2": return Brushes.Peru;
                    case "3": return Brushes.DodgerBlue;
                    default: return Brushes.MediumSeaGreen;
                }
            }
        }
    }
}

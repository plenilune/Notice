﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using wang.chuangen.Properties;

namespace wang.chuangen
{
    /// <summary>
    /// Main.xaml 的交互逻辑
    /// </summary>
    public partial class Main : Window
    {
        private static TimeSpan MIN_INTERVAL = new TimeSpan(0, 0, 5);

        private DispatcherTimer refreshTimer = new DispatcherTimer();
        private DispatcherTimer unselectTimer = new DispatcherTimer();

        private DateTime lastRefreshTime = DateTime.MinValue;
        private Brush selectedBrush = Brushes.OrangeRed;
        private Label selected;

        public Main()
        {
            InitializeComponent();
            Left = Properties.Settings.Default.WindowLeft;
            Top = Properties.Settings.Default.WindowTop;
            refreshTimer.Tick += new EventHandler(onRefreshTick);
            unselectTimer.Tick += new EventHandler(onUnselectTick);
            unselectTimer.Interval = new TimeSpan(0, 1, 0);
            itemList.DataContext = null;
            mainPanel.HorizontalAlignment = HorizontalAlignment.Left;
        }

        /// <summary>
        /// 确认弹出配置框
        /// </summary>
        /// <returns></returns>
        private bool isOpenConfig()
        {
            return MessageBox.Show("尚未配置禅道账号信息,您要现在配置吗?", "提示", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes;
        }

        /// <summary>
        /// 窗口渲染完成之后
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onContentRendered(object sender, EventArgs args)
        {
            if (Constant.isConfigOK())
            {
                try
                {
                    Constant.login(Constant.BASE_ADDRESS, Constant.ACCOUNT, Constant.PASSWORD, resetRefreshTick, Constant.emptyAction);
                }
                catch (Exception e)
                {
                    MessageBox.Show("登录失败:" + e.Message, "错误", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else if (isOpenConfig())
            {
                new Config(this, resetRefreshTick).ShowDialog();
            }
        }

        /// <summary>
        /// 打开系统配置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openConfigForm(object sender, RoutedEventArgs e)
        {
            new Config(this, resetRefreshTick).ShowDialog();
        }

        /// <summary>
        /// 手动刷新事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onRefresh(object sender, RoutedEventArgs e)
        {
            if (Constant.isConfigOK())
            {
                restartRefreshTick();
            }
            else if (isOpenConfig())
            {
                new Config(this, resetRefreshTick).ShowDialog();
            }
        }

        /// <summary>
        /// 定时任务事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onRefreshTick(object sender, EventArgs e)
        {
            this.Topmost = true;
            refreshData(Constant.emptyAction);
        }

        /// <summary>
        /// 启动定时器任务
        /// </summary>
        private void resetRefreshTick()
        {
            openedLabel.Content = 0;
            openedLabel.DataContext = null;
            bugLabel.Content = 0;
            bugLabel.DataContext = null;
            taskLabel.Content = 0;
            taskLabel.DataContext = null;
            storyLabel.Content = 0;
            storyLabel.DataContext = null;
            restartRefreshTick();
        }

        /// <summary>
        /// 重启定时任务并刷新数据
        /// </summary>
        private void restartRefreshTick()
        {
            if (refreshTimer.IsEnabled)
            {
                refreshTimer.Stop();
            }
            lastRefreshTime = DateTime.MinValue;
            refreshData(startRefreshTick);
        }

        /// <summary>
        /// 启动定时任务
        /// </summary>
        private void startRefreshTick()
        {
            refreshTimer.Interval = new TimeSpan(0, Constant.FLUSH_INTERVAL, 0);
            refreshTimer.Start();
        }

        /// <summary>
        /// 更新数据事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void refreshData(Action action)
        {
            DateTime nowTime = DateTime.Now;
            TimeSpan interval = nowTime - lastRefreshTime;
            if (interval > MIN_INTERVAL)
            {
                lastRefreshTime = nowTime;
                opened(() => bug(() => task(() => story(action))));
            }
        }

        /// <summary>
        /// 我创建的BUG
        /// </summary>
        /// <param name="action"></param>
        private void opened(Action action)
        {
            if (Constant.FLUSH_OPENED)
            {
                Constant.service("my", "bug", "openedBy", result => refreshView(openedLabel, result, "bug", false, action));
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// 我的BUG
        /// </summary>
        /// <param name="action"></param>
        private void bug(Action action)
        {
            if (Constant.FLUSH_BUG)
            {
                Constant.service("my", "bug", result => refreshView(bugLabel, result, "bug", true, action));
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// 我的任务
        /// </summary>
        /// <param name="action"></param>
        private void task(Action action)
        {
            if (Constant.FLUSH_TASK)
            {
                Constant.service("my", "task", result => refreshView(taskLabel, result, "task", false, action));
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// 我的需求
        /// </summary>
        /// <param name="action"></param>
        private void story(Action action)
        {
            if (Constant.FLUSH_STORY)
            {
                Constant.service("my", "story", result => refreshView(storyLabel, result, "story", false, action));
            }
            else
            {
                action();
            }
        }

        /// <summary>
        /// 刷新前端展示数据
        /// </summary>
        /// <param name="data"></param>
        private void refreshView(Label label, Result result, string view, bool check, Action action)
        {
            if (result != null && result.pager != null)
            {
                int total = result.pager.recTotal;
                label.Content = total;
                ICollection<Data> dataList = result.items;
                if (dataList != null && dataList.Count > 0)
                {
                    label.DataContext = dataList;
                    foreach (var item in dataList)
                    {
                        item.view = view;
                        if(item.title != null)
                        {
                            item.title = item.title.Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&");

                        }
                    }
                }
                else
                {
                    label.DataContext = null;
                }
                if (label == selected)
                {
                    if (itemList != null && dataList.Count > 0)
                    {
                        itemList.DataContext = dataList;
                    }
                    else
                    {
                        itemList.Visibility = Visibility.Collapsed;
                        itemList.DataContext = null;
                        label.BorderBrush = label.Background;
                        selected = null;
                    }
                }
                if (check)
                {
                    if (total < 1)
                    {
                        label.Background = Brushes.WhiteSmoke;
                    }
                    else if (total < 5)
                    {
                        label.Background = Brushes.Orange;
                    }
                    else if (total < 10)
                    {
                        label.Background = Brushes.Tomato;
                    }
                    else
                    {
                        label.Background = Brushes.Red;
                    }
                    if (label.BorderBrush != selectedBrush)
                    {
                        label.BorderBrush = label.Background;
                    }
                }
                action();
            }
        }

        /// <summary>
        /// 双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Label label = sender as Label;
            if (label != null)
            {
                if (selected != null)
                {
                    selected.BorderBrush = selected.Background;
                }
                ICollection<Data> dataList = getItemList(label);
                if (dataList != null && dataList != itemList.DataContext)
                {
                    selected = label;
                    label.BorderBrush = selectedBrush;
                    itemList.DataContext = dataList;
                    itemList.Visibility = Visibility.Visible;
                }
                else
                {
                    itemList.Visibility = Visibility.Collapsed;
                    itemList.DataContext = null;
                    label.BorderBrush = label.Background;
                    selected = null;
                }
            }
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="sender"></param>
        /// <returns></returns>
        private ICollection<Data> getItemList(Label label)
        {
            if (label != null && label.DataContext != null)
            {
                ICollection<Data> dataList = label.DataContext as ICollection<Data>;
                if (dataList != null && dataList.Count > 0)
                {
                    return dataList;
                }
            }
            return null;
        }

        /// <summary>
        /// 定时取消选中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onUnselectTick(object sender, EventArgs e)
        {
            unselectTimer.Stop();
            itemList.Visibility = Visibility.Collapsed;
            itemList.DataContext = null;
            if (selected != null)
            {
                selected.BorderBrush = selected.Background;
                selected = null;
            }
        }

        /// <summary>
        /// 鼠标进入窗口范围事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onMouseEnterWindow(object sender, MouseEventArgs e)
        {
            if (unselectTimer.IsEnabled)
            {
                unselectTimer.Stop();
            }
        }

        /// <summary>
        /// 鼠标离开窗口范围事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onMouseLeaveWindow(object sender, MouseEventArgs e)
        {
            if (selected != null)
            {
                if (unselectTimer.IsEnabled)
                {
                    unselectTimer.Stop();
                }
                unselectTimer.Start();
            }
        }

        /// <summary>
        /// 鼠标进入事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onMouseEnter(object sender, MouseEventArgs e)
        {
            Label label = sender as Label;
            if (label != null)
            {
                label.Background = Brushes.Aqua;
            }
        }

        /// <summary>
        /// 鼠标离开事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onMouseLeave(object sender, MouseEventArgs e)
        {
            Label label = sender as Label;
            if (label != null)
            {
                label.Background = Brushes.Azure;
            }
        }

        /// <summary>
        /// 打开浏览器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onOpenView(object sender, MouseButtonEventArgs e)
        {
            Label label = sender as Label;
            if (label != null && label.DataContext != null)
            {
                Data data = label.DataContext as Data;
                if (data != null)
                {
                    Constant.openView(data);
                }
            }
        }

        /// <summary>
        /// 鼠标拖动事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onDragMove(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.DragMove();
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// 退出事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onClose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 关闭之后事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onClosed(object sender, EventArgs e)
        {
            Settings defaultSertings = Properties.Settings.Default;
            defaultSertings.WindowLeft = (int)Left;
            defaultSertings.WindowTop = (int)Top;
            defaultSertings.Save();
            Constant.Dispose();
        }
    }
}

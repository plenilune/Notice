﻿
namespace wang.chuangen
{
    public class GetApi : Api
    {

        /// <summary>
        /// 获取接口
        /// </summary>
        /// <param name="module">模块</param>
        /// <param name="method">方法</param>
        /// <returns>url</returns>
        public string getApi(string module, string method)
        {
            return "index.php?m=" + module + "&f=" + method + "&t=json";
        }

        /// <summary>
        /// 获取接口
        /// </summary>
        /// <param name="module">模块</param>
        /// <param name="method">方法</param>
        /// <param name="type">类型</param>
        /// <returns>url</returns>
        public string getApi(string module, string method, string type)
        {
            return "index.php?m=" + module + "&f=" + method + "&type=" + type + "&t=json";
        }

        /// <summary>
        /// 获取web页面url
        /// </summary>
        /// <param name="view">view模块</param>
        /// <param name="id">id</param>
        /// <param name="sessionName">sessionName</param>
        /// <param name="sessionId">sessionId</param>
        /// <returns></returns>
        public string getWebUrl(string view, string id, string sessionName, string sessionId)
        {
            return "index.php?m=" + view + "&f=view&t=html&id=" + id + "&" + sessionName + "=" + sessionId;
        }
    }
}

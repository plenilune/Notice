﻿
namespace wang.chuangen
{
    public interface Api
    {
        /// <summary>
        /// 获取接口
        /// </summary>
        /// <param name="module">模块</param>
        /// <param name="method">方法</param>
        /// <returns>url</returns>
        string getApi(string module, string method);

        /// <summary>
        /// 获取接口
        /// </summary>
        /// <param name="module">模块</param>
        /// <param name="method">方法</param>
        /// <param name="type">类型</param>
        /// <returns>url</returns>
        string getApi(string module, string method, string type);

        /// <summary>
        /// 获取web页面url
        /// </summary>
        /// <param name="view">view模块</param>
        /// <param name="id">id</param>
        /// <param name="sessionName">sessionName</param>
        /// <param name="sessionId">sessionId</param>
        /// <returns></returns>
        string getWebUrl(string view, string id, string sessionName, string sessionId);
    }
}

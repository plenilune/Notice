﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;

namespace wang.chuangen
{

    public class ResultDeviant
    {
        private static DataContractJsonSerializer serializer = getSerializer();

        public string title { get; set; }

        public IDictionary<string, Data> items { get; set; }

        public IDictionary<string, Data> bugs { get { return items; } set { items = value; } }

        public IDictionary<string, Data> tasks { get { return items; } set { items = value; } }

        public IDictionary<string, Data> stories { get { return items; } set { items = value; } }

        public Pager pager { get; set; }

        private static DataContractJsonSerializer getSerializer()
        {
            DataContractJsonSerializerSettings settings = new DataContractJsonSerializerSettings();
            settings.UseSimpleDictionaryFormat = true;
            return new DataContractJsonSerializer(typeof(ResultDeviant), settings);
        }

        /// <summary>
        /// 解析特殊数据
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static Result convert(byte[] buffer)
        {
            try
            {
                if (buffer != null && buffer.Length > 0)
                {
                    ResultDeviant resultDeviant = serializer.ReadObject(new MemoryStream(buffer)) as ResultDeviant;
                    if (resultDeviant != null && resultDeviant.items != null)
                    {
                        Result result = new Result();
                        result.title = resultDeviant.title;
                        result.items = resultDeviant.items.Values;
                        result.pager = resultDeviant.pager;
                        return result;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }
    }
}
